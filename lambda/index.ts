export const handler = async (event: any, context: any) => {
    const response = {
      statusCode: 200,
      body: JSON.stringify({ message: 'Hello from Lambda!' }),
    };
    return response;
  };
  