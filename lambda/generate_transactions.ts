export const handler = async (event: any, context: any) => {
    // Your logic to generate transactions goes here
    const transactions = [
      { id: 1, amount: 100, description: 'Transaction 1' },
      { id: 2, amount: 200, description: 'Transaction 2' },
    ];
  
    const response = {
      statusCode: 200,
      body: JSON.stringify({ transactions }),
    };
    return response;
  };
  