//import * as faker from 'faker';
import {faker} from '@faker-js/faker'; 

interface Transaction {
  id: string;
  date: Date;
  description: string;
  amount: string;
  currency: string;
}

function generateTransaction(): Transaction {
  return {
    id: faker.datatype.uuid(),
    date: faker.date.recent(),
    description: faker.finance.transactionDescription(),
    amount: faker.finance.amount(),
    currency: 'USD',
  };
}

interface Event {
  numberOfTransactions?: number;
}

export const handler = async (event: Event) => {
  const numberOfTransactions = event.numberOfTransactions || 10;
  const transactions: Transaction[] = [];

  for (let i = 0; i < numberOfTransactions; i++) {
    transactions.push(generateTransaction());
  }

  return {
    statusCode: 200,
    body: JSON.stringify(transactions),
  };
};
