import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as apigateway from 'aws-cdk-lib/aws-apigateway';

// import * as sqs from 'aws-cdk-lib/aws-sqs';
export class MyCdkProjectStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

// Create a Lambda function
const myLambda = new lambda.Function(this, 'MyLambdaFunction', {
  runtime: lambda.Runtime.NODEJS_14_X,
  handler: 'index.handler',
  code: lambda.Code.fromAsset('lambda'),
});

const generateTransactionsLambda = new lambda.Function(this, 'GenerateTransactionsLambda', {
  runtime: lambda.Runtime.NODEJS_14_X,
  handler: 'generate_transactions.handler',
  code: lambda.Code.fromAsset('lambda'),
});

const generatePayments = new lambda.Function(this, 'generatePayments', {
  runtime: lambda.Runtime.NODEJS_14_X,
  handler: 'generate_payments.handler',
  code: lambda.Code.fromAsset('lambda'),
});

// Create an API Gateway REST API
const api = new apigateway.RestApi(this, 'MyApi', {
  restApiName: 'My Lambda API',
});
// Create a resource and add a Lambda integration
const myLambdaIntegration = new apigateway.LambdaIntegration(myLambda);
api.root.addResource('my-lambda').addMethod('GET', myLambdaIntegration);

const generateTransactionsIntegration = new apigateway.LambdaIntegration(generateTransactionsLambda);
api.root.addResource('generate-transactions').addMethod('POST', generateTransactionsIntegration);

const generatePaymentsIntegration = new apigateway.LambdaIntegration(generatePayments);
api.root.addResource('generate-payments').addMethod('POST', generatePaymentsIntegration);


  }
}
